package com.timeOfThrones.model;

public class BallotMessage {

    private final Kingdom sender;
    private final Kingdom receiver;
    private final String message;

    public BallotMessage(Kingdom sender, Kingdom receiver, String message) {
        this.sender = sender;
        this.receiver = receiver;
        this.message = message;
    }

    public Kingdom getSender() {
        return sender;
    }

    public Kingdom getReceiver() {
        return receiver;
    }

    public String getMessage() {
        return message;
    }
}
