package com.timeOfThrones.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.timeOfThrones.Constants.MINIMUM_BALLOT_SIZE;

public class Ballot {

    private final List<BallotMessage> messages = new ArrayList<>();

    public void addMessage(BallotMessage ballotMessage) {
        messages.add(ballotMessage);
    }

    public Optional<BallotMessage> getMessageAt(int index) {
        if (isInvalidIndex(index)) {
            return Optional.empty();
        }

        return Optional.of(messages.remove(index));
    }

    private boolean isInvalidIndex(int index) {
        return index >= messages.size() || index < MINIMUM_BALLOT_SIZE;
    }

    public int ballotSize() {
        return messages.size();
    }
}
