package com.timeOfThrones.model;

import com.timeOfThrones.input.Input;
import com.timeOfThrones.util.RulerValidator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.timeOfThrones.Constants.FIRST_MESSAGE_PICK;
import static com.timeOfThrones.Constants.MAXIMUM_NO_OF_PICKS;
import static com.timeOfThrones.Constants.MINIMUM_BALLOT_SIZE;

public class HighPriest {

    private Input input;

    public HighPriest(Input input) {
        this.input = input;
    }

    public String getKingdomNominations() {
        return input.get();
    }

    public void processBallot(Ballot ballot) {
        List<BallotMessage> pickedBallotMessages = new ArrayList<>();
        pickMaximumSixBallotMessages(ballot, pickedBallotMessages);
        pickedBallotMessages.forEach(ballotMessage -> {
            Kingdom receiver = ballotMessage.getReceiver();
            if (isSenderAllieWonReceiverAllie(ballotMessage, receiver)) {
                receiver.setAllegianceGivenAlready(true);
                ballotMessage.getSender().wonAllie(receiver);
            }
        });
    }

    private boolean isSenderAllieWonReceiverAllie(BallotMessage ballotMessage, Kingdom receiver) {
        return !receiver.isAllegianceGivenAlready() &&
                RulerValidator.isValidMessageForKingdom(
                        receiver, ballotMessage.getMessage());
    }

    private void pickMaximumSixBallotMessages(Ballot ballot, List<BallotMessage> pickedBallotMessages) {
        Random random = new Random();
        for (int nThMessagePick = FIRST_MESSAGE_PICK; nThMessagePick <= MAXIMUM_NO_OF_PICKS; nThMessagePick++) {
            int ballotSize = ballot.ballotSize();
            if (ballotSize <= MINIMUM_BALLOT_SIZE) {
                break;
            }
            ballot.getMessageAt(random.nextInt(ballotSize))
                    .ifPresent(pickedBallotMessages::add);
        }
    }
}
