package com.timeOfThrones.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.timeOfThrones.Constants.DELIMITER_TO_ADD_ALLIES;

public class Kingdom {

    private String name;
    private String emblem;
    private boolean isRuler;
    private boolean isAllegianceGivenAlready;
    private final List<Kingdom> allies = new ArrayList<>();

    public Kingdom(String name, String emblem) {
        this.name = name;
        this.emblem = emblem;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        return this.name.equals(o.toString());
    }

    public String getEmblem() {
        return emblem;
    }

    public boolean isRuler() {
        return isRuler;
    }

    public void setRuler(boolean ruler) {
        isRuler = ruler;
    }

    public boolean isAllegianceGivenAlready() {
        return isAllegianceGivenAlready;
    }

    public void setAllegianceGivenAlready(boolean allegianceGivenAlready) {
        isAllegianceGivenAlready = allegianceGivenAlready;
    }

    public String allies() {
        return allies.stream()
                .map(Object::toString)
                .collect(Collectors.joining(DELIMITER_TO_ADD_ALLIES));
    }

    public void wonAllie(Kingdom allie) {
        this.allies.add(allie);
    }

    public int alliesCount() {
        return allies.size();
    }
}
