package com.timeOfThrones.model;

import com.timeOfThrones.input.Input;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.timeOfThrones.Constants.DELIMITER_TO_ADD_ALLIES;
import static com.timeOfThrones.Constants.PREFIX_KING;

public class King {

    private String name;
    private Input input;
    private boolean isRuler;
    private List<Kingdom> allies = new ArrayList<>();
    private int kingdomSupportNeededCount;

    public King(String name, Input input) {
        this.name = name;
        this.input = input;
        this.kingdomSupportNeededCount = Integer.MAX_VALUE;
    }

    public King(String name, Input input, int kingdomSupportNeededCount) {
        this.name = name;
        this.input = input;
        this.kingdomSupportNeededCount = kingdomSupportNeededCount;
    }

    @Override
    public String toString() {
        return PREFIX_KING + name;
    }

    public String getRecipientAndSecretMessage() {
        return input.get();
    }

    public boolean isRuler() {
        return isRuler;
    }

    public void setRuler(boolean isRuler) {
        this.isRuler = isRuler;
    }

    public void wonAllie(Kingdom kingdom) {
        if (isKingdomAlreadyPresent(kingdom)) {
            return;
        }
        kingdomSupportNeededCount--;
        this.allies.add(kingdom);
    }

    private boolean isKingdomAlreadyPresent(Kingdom kingdom) {
        return allies.contains(kingdom);
    }

    public String allies() {
        return allies.stream().map(Object::toString).collect(Collectors.joining(DELIMITER_TO_ADD_ALLIES));
    }

    public int getKingdomSupportNeededCount() {
        return kingdomSupportNeededCount;
    }

}
