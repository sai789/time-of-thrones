package com.timeOfThrones.service;

import com.timeOfThrones.output.Output;

import static com.timeOfThrones.Constants.ALLIES_OF_RULER_MESSAGE;
import static com.timeOfThrones.Constants.WHO_IS_THE_RULER_MESSAGE;

public abstract class RulerFindService {

    protected final Output output;

    protected RulerFindService(Output output) {
        this.output = output;
    }

    abstract void start();

    abstract String whoIsRuler();

    abstract String rulersAllies();

    protected void status() {
        output.display(WHO_IS_THE_RULER_MESSAGE);
        output.display(whoIsRuler());
        output.display(ALLIES_OF_RULER_MESSAGE);
        output.display(rulersAllies());
    }

}
