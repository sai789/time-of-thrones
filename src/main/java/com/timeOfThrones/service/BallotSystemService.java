package com.timeOfThrones.service;

import com.timeOfThrones.model.Ballot;
import com.timeOfThrones.model.BallotMessage;
import com.timeOfThrones.model.HighPriest;
import com.timeOfThrones.model.Kingdom;
import com.timeOfThrones.output.Output;
import com.timeOfThrones.parser.Parser;
import com.timeOfThrones.util.RandomMessageProvider;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.timeOfThrones.Constants.COMPETING_ALLIES_WON_STATUS;
import static com.timeOfThrones.Constants.KINGDOMS_COMPETING_TO_BE_THE_RULER;
import static com.timeOfThrones.Constants.NONE;
import static com.timeOfThrones.Constants.RESULTS_AFTER_BALLOT_COUNT;
import static com.timeOfThrones.Constants.SINGLETON_LIST_SIZE;
import static com.timeOfThrones.Constants.WON_NO_ALLIES;

public class BallotSystemService extends RulerFindService implements RulerFindStrategy {

    private final HighPriest highPriest;
    private final Ballot ballot;
    private Kingdom rulerKingdom;
    private static int ballotRound = 1;

    public BallotSystemService(Output output, HighPriest highPriest, Ballot ballot) {
        super(output);
        this.highPriest = highPriest;
        this.ballot = ballot;
    }

    @Override
    public void start() {
        status();
        startRulerFindProcess();
        status();
    }

    @Override
    String whoIsRuler() {
        return Objects.isNull(rulerKingdom) ? NONE : rulerKingdom.toString();
    }

    @Override
    String rulersAllies() {
        return Objects.isNull(rulerKingdom) ? NONE : rulerKingdom.allies();
    }

    @Override
    public void startRulerFindProcess() {
        String kingdomNominations = getCompetingKingdomsFromHighPriest();
        kingdomNominations = kingdomNominations.trim();
        if (kingdomNominations.isEmpty()) {
            return;
        }
        List<Kingdom> competingKingdoms =
                Parser.getCompetingKingdomsFromPriestInput(kingdomNominations);
        if (competingKingdoms.isEmpty()) {
            return;
        }
        List<Kingdom> nonCompetingKingdoms =
                Parser.getNonCompetingKingdomsFromPriestInput(kingdomNominations);
        repeatBallotProcessTillNoTieOccur(competingKingdoms, nonCompetingKingdoms);
    }

    private void repeatBallotProcessTillNoTieOccur(
            List<Kingdom> competingKingdoms, List<Kingdom> nonCompetingKingdoms) {
        do {
            addMessagesFromAllCompeteKingdomsToAllNonCompete(competingKingdoms, nonCompetingKingdoms);
            highPriest.processBallot(ballot);
            findBallotResults(competingKingdoms, nonCompetingKingdoms);
        }
        while (Objects.isNull(rulerKingdom));
    }

    private void findBallotResults(List<Kingdom> competingKingdoms, List<Kingdom> nonCompetingKingdoms) {
        printWonAlliesStatusForAllCompetingKingdoms(competingKingdoms);
        competingKingdoms.sort(Comparator.comparingInt(Kingdom::alliesCount).reversed());
        if (isThereNoTieAmongCompeteKingdoms(competingKingdoms)) {
            rulerKingdom = competingKingdoms.get(0);
            return;
        }
        moveNonTiedKingdomsToNonCompeteKingdomsForNextRound(competingKingdoms, nonCompetingKingdoms);
    }

    private void moveNonTiedKingdomsToNonCompeteKingdomsForNextRound(
            List<Kingdom> competingKingdoms, List<Kingdom> nonCompetingKingdoms) {
        List<Kingdom> nonTiedKingdoms = getNonTiedKingdoms(competingKingdoms);
        nonCompetingKingdoms.addAll(nonTiedKingdoms);
        competingKingdoms.removeAll(nonTiedKingdoms);
    }

    private List<Kingdom> getNonTiedKingdoms(List<Kingdom> competingKingdoms) {
        int maxAlliesWonCount = competingKingdoms.get(0).alliesCount();
        return competingKingdoms.stream()
                .filter(competeKingdom -> competeKingdom.alliesCount() < maxAlliesWonCount)
                .collect(Collectors.toList());
    }

    private boolean isThereNoTieAmongCompeteKingdoms(List<Kingdom> competingKingdoms) {
        if (isSingletonList(competingKingdoms)) {
            return competingKingdoms.get(0).alliesCount() > WON_NO_ALLIES;
        }
        return competingKingdoms.get(0).alliesCount() >
                competingKingdoms.get(1).alliesCount();
    }

    private boolean isSingletonList(List<Kingdom> competingKingdoms) {
        return competingKingdoms.size() == SINGLETON_LIST_SIZE;
    }

    private void printWonAlliesStatusForAllCompetingKingdoms(List<Kingdom> competingKingdoms) {
        output.display(getBallotResultsMessageForCurrentRound());
        competingKingdoms.forEach(competingKingdom ->
                output.display(getMessageForCompetingAlliesWonStatus(competingKingdom)));
    }

    private String getBallotResultsMessageForCurrentRound() {
        return String.format(RESULTS_AFTER_BALLOT_COUNT, ballotRound++);
    }

    private String getMessageForCompetingAlliesWonStatus(Kingdom competingKingdom) {
        return String.format(COMPETING_ALLIES_WON_STATUS, competingKingdom, competingKingdom.alliesCount());
    }

    private void addMessagesFromAllCompeteKingdomsToAllNonCompete(
            List<Kingdom> competingKingdoms, List<Kingdom> nonCompetingKingdoms) {
        for (Kingdom competeKingdom : competingKingdoms) {
            addMessagesToAllNonCompeteKingdoms(competeKingdom, nonCompetingKingdoms);
        }
    }

    private void addMessagesToAllNonCompeteKingdoms(
            Kingdom competeKingdom, List<Kingdom> nonCompetingKingdoms) {
        for (Kingdom nonCompeteKingdom : nonCompetingKingdoms) {
            String secretMessage = RandomMessageProvider.get();
            ballot.addMessage(new BallotMessage(competeKingdom, nonCompeteKingdom, secretMessage));
        }
    }

    private String getCompetingKingdomsFromHighPriest() {
        output.display(KINGDOMS_COMPETING_TO_BE_THE_RULER);
        return highPriest.getKingdomNominations();
    }
}
