package com.timeOfThrones.service;

import com.timeOfThrones.exception.UnknownKingdomException;
import com.timeOfThrones.model.King;
import com.timeOfThrones.model.Kingdom;
import com.timeOfThrones.output.Output;
import com.timeOfThrones.parser.Parser;
import com.timeOfThrones.util.RulerValidator;

import static com.timeOfThrones.Constants.INPUT_MESSAGES_TO_KINGDOMS_FROM_KING;
import static com.timeOfThrones.Constants.LINE_SEPARATOR;
import static com.timeOfThrones.Constants.NONE;
import static com.timeOfThrones.Constants.NO_MORE_SUPPORT_NEEDED;

//should start the process to find, i.e print status, ask for messages
public class OneKingRulerFindService extends RulerFindService implements RulerFindStrategy {

    private Output output;
    private King king;

    public OneKingRulerFindService(Output output, King king) {
        super(output);
        this.output = output;
        this.king = king;
    }

    @Override
    public void start() {
        status();
        startRulerFindProcess();
        status();
    }

    @Override
    String whoIsRuler() {
        return king.isRuler() ? king.toString() + LINE_SEPARATOR : NONE;
    }

    @Override
    String rulersAllies() {
        return king.isRuler() ? king.allies() + LINE_SEPARATOR : NONE;
    }

    @Override
    public void startRulerFindProcess() {
        output.display(String.format(INPUT_MESSAGES_TO_KINGDOMS_FROM_KING, king.toString()));
        String kingsRecipientAndSecretMessage = getInputFromKing();
        while (!kingsRecipientAndSecretMessage.isEmpty()) {
            doValidate(kingsRecipientAndSecretMessage);
            kingsRecipientAndSecretMessage = getInputFromKing();
        }
    }

    private void doValidate(String kingsRecipientAndSecretMessage) {
        Kingdom kingdom;
        try {
            kingdom = Parser.getKingdomFromKingsMessage(kingsRecipientAndSecretMessage);
        } catch (UnknownKingdomException exception) {
            return;
        }

        String message = Parser.extractKingsMessage(kingsRecipientAndSecretMessage);
        if (RulerValidator.isValidMessageForKingdom(kingdom, message)) {
            king.wonAllie(kingdom);
        }
        if (king.getKingdomSupportNeededCount() == NO_MORE_SUPPORT_NEEDED) {
            king.setRuler(true);
        }
    }

    private String getInputFromKing() {
        return king.getRecipientAndSecretMessage();
    }

}
