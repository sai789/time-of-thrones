package com.timeOfThrones;

import com.timeOfThrones.input.ConsoleInput;
import com.timeOfThrones.input.Input;
import com.timeOfThrones.model.Ballot;
import com.timeOfThrones.model.HighPriest;
import com.timeOfThrones.output.ConsoleOutput;
import com.timeOfThrones.output.Output;
import com.timeOfThrones.service.BallotSystemService;

public class Application {
    public static void main(String[] args) {
        Input input = new ConsoleInput(System.in);
        Output output = new ConsoleOutput(System.out);

        new BallotSystemService(output, new HighPriest(input), new Ballot()).start();
    }
}
