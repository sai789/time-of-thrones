package com.timeOfThrones;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Constants {
    public static final String ALLIES_OF_RULER_MESSAGE = "\nAllies of Ruler?\n";
    public static final String WHO_IS_THE_RULER_MESSAGE = "\n\nWho is the ruler of Southeros?\n";
    public static final String NONE = "None\n";
    public static final String INPUT_MESSAGES_TO_KINGDOMS_FROM_KING = "Input Messages to kingdoms from %s:(press ENTER to stop entering inputs)";
    public static final String KINGDOMS_COMPETING_TO_BE_THE_RULER = "Enter the kingdoms competing to be the ruler:(press ENTER quit)";
    public static final String UNKNOWN_KINGDOM_EXCEPTION_MESSAGE = "Given kingdom %s, is not a valid kingdom name";
    public static final String COMPETING_ALLIES_WON_STATUS = "\nAllies for %s: %d\n";
    public static final String RESULTS_AFTER_BALLOT_COUNT = "\nResults after round %d ballot count\n";
    public static final String PREFIX_KING = "King ";
    public static final int NO_MORE_SUPPORT_NEEDED = 0;
    public static final String LINE_SEPARATOR = "\n";
    public static final int NOT_FOUND_INDEX = -1;
    public static final int OFFSET_TO_MOVE_NEXT_CHARACTER = 1;
    public static final String EMPTY_STRING_TO_REPLACE = "";
    public static final int BEGIN_INDEX = 0;
    public static final int MINIMUM_BALLOT_SIZE = 0;
    public static final int WON_NO_ALLIES = 0;
    public static final int FIRST_MESSAGE_PICK = 1;
    public static final int SINGLETON_LIST_SIZE = 1;
    public static final int MAXIMUM_NO_OF_PICKS = 6;
    public static final char DOUBLE_QUOTE = '"';
    public static final String COMMA = ",";
    public static final String SPACE = " ";
    public static final String DELIMITER_TO_ADD_ALLIES = ", ";
    public static final List<String> EXISTING_KINGDOMS =
            Arrays.asList("Air", "Space", "Water", "Ice", "Land", "Fire");
    public static final Map<String, String> NAME_TO_EMBLEM_MAPPER =
            new HashMap<String, String>() {{
                put("Air", "Owl");
                put("Space", "Gorilla");
                put("Water", "Octopus");
                put("Ice", "Mammoth");
                put("Land", "Panda");
                put("Fire", "Dragon");
            }};
    public static final List<String> SECRET_MESSAGES = Arrays.asList(
            "Summer is coming",
            "a1d22n333a4444p",
            "oaaawaala",
            "zmzmzmzaztzozh",
            "Go, risk it all",
            "Let's swing the sword together",
            "Die or play the tame of thrones",
            "Ahoy! Fight for me with men and money",
            "Drag on Martin!",
            "When you play the tame of thrones, you win or you die.",
            "What could we say to the Lord of Death? Game on?",
            "Turn us away, and we will burn you first",
            "Death is so terribly final, while life is full of possibilities.",
            "You Win or You Die",
            "His watch is Ended",
            "Sphinx of black quartz, judge my dozen vows",
            "Fear cuts deeper than swords, My Lord.",
            "Different roads sometimes lead to the same castle.",
            "A DRAGON IS NOT A SLAVE.",
            "Do not waste paper",
            "Go ring all the bells",
            "Crazy Fredrick bought many very exquisite pearl, emerald and diamond jewels.",
            "The quick brown fox jumps over a lazy dog multiple times.",
            "We promptly judged antique ivory buckles for the next prize.",
            "Walar Morghulis: All men must die."
    );
}
