package com.timeOfThrones.util;

import com.timeOfThrones.exception.UnknownKingdomException;
import com.timeOfThrones.model.Kingdom;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.timeOfThrones.Constants.EXISTING_KINGDOMS;
import static com.timeOfThrones.Constants.NAME_TO_EMBLEM_MAPPER;
import static com.timeOfThrones.Constants.UNKNOWN_KINGDOM_EXCEPTION_MESSAGE;

public class ValidKingdomBuilder {

    public static Kingdom getKingdomFor(String name) {
        String validKingdom = getMatchedKingdomName(name, EXISTING_KINGDOMS);
        if (!validKingdom.isEmpty()) {
            return new Kingdom(validKingdom, NAME_TO_EMBLEM_MAPPER.get(validKingdom));
        }
        throw new UnknownKingdomException(getExceptionMessage(name));
    }

    private static String getMatchedKingdomName(String name, List<String> inKingdomList) {
        return inKingdomList.stream()
                .filter(existingKingdomName ->
                        existingKingdomName.equalsIgnoreCase(name.trim()))
                .findAny()
                .orElse("");
    }

    private static String getExceptionMessage(String name) {
        return String.format(UNKNOWN_KINGDOM_EXCEPTION_MESSAGE, name);
    }

    public static List<Kingdom> getKingdomsOtherThan(String[] kingdoms) {
        List<String> competingKingdoms = Arrays.asList(kingdoms);
        List<Kingdom> otherKingdoms = new ArrayList<>();
        for (String validKingdomName : EXISTING_KINGDOMS) {
            String foundMatch = getMatchedKingdomName(validKingdomName, competingKingdoms);
            if (foundMatch.isEmpty()) {
                otherKingdoms.add(getKingdomFor(validKingdomName));
            }
        }
        return otherKingdoms;
    }
}
