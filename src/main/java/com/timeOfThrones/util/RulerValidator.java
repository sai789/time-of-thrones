package com.timeOfThrones.util;

import com.timeOfThrones.model.Kingdom;

import static com.timeOfThrones.Constants.EMPTY_STRING_TO_REPLACE;
import static com.timeOfThrones.Constants.NOT_FOUND_INDEX;
import static com.timeOfThrones.Constants.OFFSET_TO_MOVE_NEXT_CHARACTER;

public class RulerValidator {

    public static boolean isValidMessageForKingdom(Kingdom kingdom, String message) {
        message = message.toLowerCase();
        String emblem = kingdom.getEmblem().toLowerCase();
        for (char emblemCharacter : emblem.toCharArray()) {
            int foundEmblemCharacterIndex = message.indexOf(emblemCharacter);
            if (isCharacterNotFound(foundEmblemCharacterIndex)) {
                return false;
            }
            message = substringExcludingEmblemCharacter(message, foundEmblemCharacterIndex);
        }
        return true;
    }

    private static String substringExcludingEmblemCharacter(String message, int foundIndex) {
        return new StringBuilder(message)
                .replace(foundIndex, foundIndex + OFFSET_TO_MOVE_NEXT_CHARACTER, EMPTY_STRING_TO_REPLACE)
                .toString();
    }

    private static boolean isCharacterNotFound(int foundIndex) {
        return foundIndex == NOT_FOUND_INDEX;
    }

}
