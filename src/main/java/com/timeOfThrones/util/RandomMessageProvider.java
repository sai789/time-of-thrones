package com.timeOfThrones.util;

import java.util.Random;

import static com.timeOfThrones.Constants.SECRET_MESSAGES;

public class RandomMessageProvider {

    public static String get() {
        int noOfMessages = SECRET_MESSAGES.size();
        Random random = new Random();
        return SECRET_MESSAGES.get(random.nextInt(noOfMessages));
    }

}
