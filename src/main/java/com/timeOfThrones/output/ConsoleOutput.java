package com.timeOfThrones.output;

import java.io.PrintStream;

public class ConsoleOutput implements Output {

    private PrintStream output;

    public ConsoleOutput(PrintStream output) {
        this.output = output;
    }

    @Override
    public void display(String message) {
        output.print(message);
    }

}
