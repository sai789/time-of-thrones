package com.timeOfThrones.output;

public interface Output {
    void display(String message);
}