package com.timeOfThrones.parser;

import com.timeOfThrones.model.Kingdom;
import com.timeOfThrones.util.ValidKingdomBuilder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.timeOfThrones.Constants.BEGIN_INDEX;
import static com.timeOfThrones.Constants.COMMA;
import static com.timeOfThrones.Constants.DOUBLE_QUOTE;
import static com.timeOfThrones.Constants.NOT_FOUND_INDEX;
import static com.timeOfThrones.Constants.OFFSET_TO_MOVE_NEXT_CHARACTER;
import static com.timeOfThrones.Constants.SPACE;

public class Parser {

    public static String extractKingsMessage(String information) {
        int firstFoundIndex = information.indexOf(DOUBLE_QUOTE);
        int lastFoundIndex = information.lastIndexOf(DOUBLE_QUOTE);
        if (isCharacterNotFound(firstFoundIndex)) {
            firstFoundIndex = findCommaOrSpaceFoundIndex(information);
            lastFoundIndex = information.length();
        } else if (isCharacterFoundOnlyOnce(firstFoundIndex, lastFoundIndex)) {
            lastFoundIndex = information.length();
        }
        return information.substring(firstFoundIndex + OFFSET_TO_MOVE_NEXT_CHARACTER, lastFoundIndex);
    }

    private static int findCommaOrSpaceFoundIndex(String information) {
        int commaFoundIndex = information.indexOf(COMMA);
        return isCharacterNotFound(commaFoundIndex) ?
                information.indexOf(SPACE) : commaFoundIndex;
    }

    private static boolean isCharacterFoundOnlyOnce(int firstFoundIndex, int lastFoundIndex) {
        return firstFoundIndex == lastFoundIndex;
    }

    private static boolean isCharacterNotFound(int foundIndex) {
        return foundIndex == NOT_FOUND_INDEX;
    }

    public static Kingdom getKingdomFromKingsMessage(String information) {
        int commaFoundIndex = findCommaOrSpaceFoundIndex(information);
        if (isCharacterNotFound(commaFoundIndex)) {
            return ValidKingdomBuilder.getKingdomFor(information);
        }
        return ValidKingdomBuilder.getKingdomFor(
                information.substring(BEGIN_INDEX, commaFoundIndex));
    }

    public static List<Kingdom> getCompetingKingdomsFromPriestInput(String competingKingdoms) {
        List<Kingdom> kingdoms = new ArrayList<>();
        Set<String> uniqueKingdoms = new HashSet<>();
        Collections.addAll(uniqueKingdoms, splitKingdomsBySpaceOrComma(competingKingdoms));
        for (Object kingdom : uniqueKingdoms) {
            kingdoms.add(ValidKingdomBuilder.getKingdomFor((String) kingdom));
        }
        return kingdoms;
    }

    private static String[] splitKingdomsBySpaceOrComma(String competingKingdoms) {
        return isCharacterNotFound(competingKingdoms.indexOf(SPACE)) ?
                competingKingdoms.split(COMMA) : competingKingdoms.split(SPACE);
    }

    public static List<Kingdom> getNonCompetingKingdomsFromPriestInput(String competingKingdoms) {
        String[] kingdoms = splitKingdomsBySpaceOrComma(competingKingdoms);
        return ValidKingdomBuilder.getKingdomsOtherThan(kingdoms);
    }
}
