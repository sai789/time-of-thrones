package com.timeOfThrones.input;

public interface Input {
    String get();
}
