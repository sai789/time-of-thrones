package com.timeOfThrones.input;

import java.io.InputStream;
import java.util.Scanner;

public class ConsoleInput implements Input {

    private Scanner scanner;

    public ConsoleInput(InputStream inputStream) {
        this.scanner = new Scanner(inputStream);
    }

    @Override
    public String get() {
        return scanner.nextLine();
    }

}
