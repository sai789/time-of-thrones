package com.timeOfThrones.exception;

public class UnknownKingdomException extends RuntimeException {
    public UnknownKingdomException(String exceptionMessage) {
        super(exceptionMessage);
    }
}
