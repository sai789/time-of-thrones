package com.timeOfThrones.input;

import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;

public class ConsoleInputTest {

    @Test
    public void shouldGetFeedInput() {
        String expectedInput = "Feed Input";
        ByteArrayInputStream inputStream = new ByteArrayInputStream(expectedInput.getBytes());
        ConsoleInput input = new ConsoleInput(inputStream);

        String actualInput = input.get();

        Assert.assertEquals(expectedInput, actualInput);
    }

}