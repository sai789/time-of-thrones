package com.timeOfThrones.parser;

import com.timeOfThrones.model.Kingdom;
import com.timeOfThrones.util.ValidKingdomBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;
import static org.powermock.api.mockito.PowerMockito.when;

@PrepareForTest(ValidKingdomBuilder.class)
@RunWith(PowerMockRunner.class)
public class ParserTest {

    @Mock
    private Kingdom kingdom;

    @Mock
    private Kingdom otherKingdom;

    @Before
    public void mockSetup() {
        mockStatic(ValidKingdomBuilder.class);
        when(ValidKingdomBuilder.getKingdomFor("Air")).thenReturn(kingdom);
        when(ValidKingdomBuilder.getKingdomFor("Fire")).thenReturn(otherKingdom);
    }

    @Test
    public void shouldParseKingsMessage() {
        assertEquals("oaaawaala",
                Parser.extractKingsMessage("Air, \"oaaawaala\"ola"));
    }

    @Test
    public void shouldParseKingsMessageToTheEndIfInformationHasOnlyOneDoubleQuote() {
        assertEquals("oaaaw",
                Parser.extractKingsMessage("Air, \"oaaaw"));
    }

    @Test
    public void shouldTakeMessageFromFirstCommaToEndIfNoDoubleQuoteThere() {
        assertEquals(" oaaaw", Parser.extractKingsMessage("Air, oaaaw"));
    }

    @Test
    public void shouldTakeMessageFromFirstSpaceToEndIfHasNoDoubleQuoteAndComma() {
        assertEquals("oaaaw re", Parser.extractKingsMessage("Air oaaaw re"));
    }

    @Test
    public void shouldContactKingdomBuilderToGetKingdomFromKingsMessage() {
        String kingsMessage = "Air, \"oaaawaala\"ola";

        Parser.getKingdomFromKingsMessage(kingsMessage);

        verifyStatic();
        ValidKingdomBuilder.getKingdomFor("Air");
    }

    @Test
    public void shouldCreateKingdomWithFirstWordOfMessageIfMessageHasNoComma() {
        String kingsMessage = "Air oaaawaala ola";

        Parser.getKingdomFromKingsMessage(kingsMessage);

        verifyStatic();
        ValidKingdomBuilder.getKingdomFor("Air");
    }

    @Test
    public void shouldCreateKingdomWithWholeMessageIfMessageHasNoCommaAndSpace() {
        String kingsMessage = "Airoaaawaalaola";

        Parser.getKingdomFromKingsMessage(kingsMessage);

        verifyStatic();
        ValidKingdomBuilder.getKingdomFor("Airoaaawaalaola");
    }

    @Test
    public void shouldGetListOfKingdomsWhichSpaceSeparatedInInformation() {
        String competingKingdoms = "Air Fire";

        List<Kingdom> kingdoms = Parser.getCompetingKingdomsFromPriestInput(competingKingdoms);

        verifyKingdoms(kingdoms);
    }

    @Test
    public void shouldGetListOfKingdomsWhichCommaSeparatedInInformationIfNotSpaceSeparated() {
        String competingKingdoms = "Air,Fire";

        List<Kingdom> kingdoms = Parser.getCompetingKingdomsFromPriestInput(competingKingdoms);

        verifyKingdoms(kingdoms);
    }

    @Test
    public void shouldGetNonCompetingKingdomsThatAreNotInPriestInput() {
        String competingKingdoms = "Air,Fire";

        Parser.getNonCompetingKingdomsFromPriestInput(competingKingdoms);

        verifyStatic();
        ValidKingdomBuilder.getKingdomsOtherThan(competingKingdoms.split(","));
    }

    @Test
    public void shouldExcludeDuplicatesFromPriestsCompeteKingdomsInput() {
        String competingKingdoms = "Air,Air";

        Parser.getCompetingKingdomsFromPriestInput(competingKingdoms);

        verifyStatic(times(1));
        ValidKingdomBuilder.getKingdomFor("Air");
    }

    private void verifyKingdoms(List<Kingdom> kingdoms) {
        verifyStatic();
        ValidKingdomBuilder.getKingdomFor("Air");
        verifyStatic();
        ValidKingdomBuilder.getKingdomFor("Fire");
        Assert.assertEquals(2, kingdoms.size());
        Assert.assertTrue(kingdoms.contains(kingdom));
        Assert.assertTrue(kingdoms.contains(otherKingdom));
    }
}
