package com.timeOfThrones.service;

import com.timeOfThrones.exception.UnknownKingdomException;
import com.timeOfThrones.input.Input;
import com.timeOfThrones.model.King;
import com.timeOfThrones.model.Kingdom;
import com.timeOfThrones.output.Output;
import com.timeOfThrones.parser.Parser;
import com.timeOfThrones.service.OneKingRulerFindService;
import com.timeOfThrones.util.RulerValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;
import static org.powermock.api.mockito.PowerMockito.when;

@PrepareForTest({Parser.class, RulerValidator.class})
@RunWith(PowerMockRunner.class)
public class OneKingRulerFindServiceTest {

    @Mock
    private Output output;

    @Mock
    private Input input;

    @Mock
    private King king;

    @Mock
    private Kingdom kingdom;

    @Before
    public void setup() {
        mockStatic(Parser.class);
        mockStatic(RulerValidator.class);
        when(king.getRecipientAndSecretMessage()).thenReturn("");
        when(input.get()).thenReturn("");
    }

    @Test
    public void shouldStartFindingByFirstShowingCurrentRulerStatus() {
        String expectedMessageToBeCalledWith = "\n\nWho is the ruler of Southeros?\n";
        OneKingRulerFindService rulerFindService = new OneKingRulerFindService(output, new King("", input));

        rulerFindService.start();

        verify(output, atLeastOnce()).display(expectedMessageToBeCalledWith);
    }

    @Test
    public void shouldShowAlliesOfRulerMessage() {
        String expectedMessageToBeCalledWith = "\nAllies of Ruler?\n";
        OneKingRulerFindService rulerFindService = new OneKingRulerFindService(output, new King("", input));

        rulerFindService.start();

        verify(output, atLeastOnce()).display(expectedMessageToBeCalledWith);
    }

    @Test
    public void shouldBeAbleToPrintKingNameIfKingIsRuler() {
        String kingName = "Shan";
        King king = new King(kingName, input);
        king.setRuler(true);
        OneKingRulerFindService rulerFindService = new OneKingRulerFindService(output, king);

        rulerFindService.start();

        verify(output, atLeastOnce()).display("King " + kingName + "\n");
    }

    @Test
    public void shouldBeAbleToAskKingForRecipientAndSecretMessage() {
        String shan = "Shan";
        when(king.toString()).thenReturn(shan);
        OneKingRulerFindService rulerFindService = new OneKingRulerFindService(output, king);

        rulerFindService.start();

        verify(king).getRecipientAndSecretMessage();
        verify(output).display("Input Messages to kingdoms from " + shan + ":(press ENTER to stop entering inputs)");
    }

    @Test
    public void shouldContactParserToParseSecretMessage() {
        String kingRecipientAndMessage = "Kingdom, message";
        when(king.getRecipientAndSecretMessage()).thenReturn(kingRecipientAndMessage).thenReturn("");
        OneKingRulerFindService rulerFindService = new OneKingRulerFindService(output, king);

        rulerFindService.start();

        verifyStatic();
        Parser.extractKingsMessage(kingRecipientAndMessage);
    }

    @Test
    public void shouldContactParserToParseKingdom() {
        String kingsRecipientAndMessage = "Kingdom, message";
        when(king.getRecipientAndSecretMessage()).thenReturn(kingsRecipientAndMessage).thenReturn("");
        OneKingRulerFindService rulerFindService = new OneKingRulerFindService(output, king);

        rulerFindService.start();

        verifyStatic();
        Parser.getKingdomFromKingsMessage(kingsRecipientAndMessage);
    }

    @Test
    public void shouldPrintNoneIfProvidedKingIsNotRuler() {
        when(king.isRuler()).thenReturn(false);
        OneKingRulerFindService rulerFindService = new OneKingRulerFindService(output, king);

        rulerFindService.start();

        verify(king, atLeastOnce()).isRuler();
        verify(output, atLeast(2)).display("None\n");
    }

    @Test
    public void shouldPrintAlliesIfProvidedKingIsRuler() {
        when(king.isRuler()).thenReturn(true);
        String expectedAllies = "all, allies, here";
        when(king.allies()).thenReturn(expectedAllies);
        OneKingRulerFindService rulerFindService = new OneKingRulerFindService(output, king);

        rulerFindService.start();

        verify(king, atLeastOnce()).allies();
        verify(output, atLeastOnce()).display(expectedAllies + "\n");
    }

    @Test
    public void shouldContactRulerValidatorForValidatingMessageForKingdom() {
        when(Parser.getKingdomFromKingsMessage(anyString())).thenReturn(kingdom);
        String extractedMessage = "extracted message";
        when(Parser.extractKingsMessage(anyString())).thenReturn(extractedMessage);
        when(king.getRecipientAndSecretMessage()).thenReturn("some message").thenReturn("");
        OneKingRulerFindService rulerFindService = new OneKingRulerFindService(output, king);

        rulerFindService.start();

        verifyStatic();
        RulerValidator.isValidMessageForKingdom(kingdom, extractedMessage);
    }

    @Test
    public void shouldSetAllieToKingIfRulerValidationIsTrueForAllie() {
        when(RulerValidator.isValidMessageForKingdom(any(), anyString())).thenReturn(true);
        when(Parser.getKingdomFromKingsMessage(anyString())).thenReturn(kingdom);
        when(king.getRecipientAndSecretMessage()).thenReturn("some message").thenReturn("");
        OneKingRulerFindService rulerFindService = new OneKingRulerFindService(output, king);

        rulerFindService.start();

        verify(king).wonAllie(kingdom);
    }

    @Test
    public void shouldSetRulerTrueIfSupportNeededCountIsZero() {
        when(king.getKingdomSupportNeededCount()).thenReturn(0);
        when(king.getRecipientAndSecretMessage()).thenReturn("some message").thenReturn("");
        OneKingRulerFindService rulerFindService = new OneKingRulerFindService(output, king);

        rulerFindService.start();

        verify(king).setRuler(true);
    }

    @Test
    public void shouldNotExitProgramButProceedToTakeNextInputIfUnknownKingdomExceptionRaised() {
        when(Parser.getKingdomFromKingsMessage(anyString())).thenThrow(new UnknownKingdomException(""));
        when(king.getRecipientAndSecretMessage()).thenReturn("some message").thenReturn("");
        OneKingRulerFindService rulerFindService = new OneKingRulerFindService(output, king);

        rulerFindService.start();

        verifyStatic(never());
        Parser.extractKingsMessage(anyString());
        verifyStatic(never());
        RulerValidator.isValidMessageForKingdom(any(), anyString());
        verify(king, times(2)).getRecipientAndSecretMessage();
    }

}