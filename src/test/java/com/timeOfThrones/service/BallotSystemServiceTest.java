package com.timeOfThrones.service;

import com.timeOfThrones.model.Ballot;
import com.timeOfThrones.model.BallotMessage;
import com.timeOfThrones.model.HighPriest;
import com.timeOfThrones.model.Kingdom;
import com.timeOfThrones.output.Output;
import com.timeOfThrones.parser.Parser;
import com.timeOfThrones.util.RandomMessageProvider;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;

@PrepareForTest({Parser.class, RandomMessageProvider.class})
@RunWith(PowerMockRunner.class)
public class BallotSystemServiceTest {

    @Mock
    private Kingdom competeKingdom;

    @Mock
    private Kingdom otherCompeteKingdom;

    @Mock
    private Kingdom anotherCompeteKingdom;

    @Mock
    private Kingdom nonCompeteKingdom;

    @Mock
    private Kingdom otherNonCompeteKingdom;

    @Mock
    private Ballot ballot;

    @Mock
    private Output output;

    @Mock
    private HighPriest highPriest;

    private BallotSystemService ballotSystemService;

    @Before
    public void setUp() {
        mockStatic(Parser.class);
        mockStatic(RandomMessageProvider.class);
        when(highPriest.getKingdomNominations()).thenReturn("competing names");
        when(Parser.getCompetingKingdomsFromPriestInput(anyString()))
                .thenReturn(prepareMutableList(new Kingdom[]{competeKingdom}));
        when(competeKingdom.alliesCount()).thenReturn(3);
        when(Parser.getNonCompetingKingdomsFromPriestInput(anyString()))
                .thenReturn(prepareMutableList(new Kingdom[]{nonCompeteKingdom}));

        ballotSystemService = new BallotSystemService(output, highPriest, ballot);
    }

    @Test
    public void shouldGetRulerNameAsNoneIfThereIsNoRuler() {
        Assert.assertEquals("None\n", ballotSystemService.whoIsRuler());
    }

    @Test
    public void shouldGetRulerAlliesAsNoneIfThereIsNoRuler() {
        Assert.assertEquals("None\n", ballotSystemService.rulersAllies());
    }

    @Test
    public void shouldPrintMessageForAskingCompetingKingdoms() {
        ballotSystemService.startRulerFindProcess();

        verify(output).display("Enter the kingdoms competing to be the ruler:(press ENTER quit)");
    }

    @Test
    public void shouldContactPriestForCompetingKingdoms() {
        ballotSystemService.startRulerFindProcess();

        verify(highPriest).getKingdomNominations();
    }

    @Test
    public void shouldContactParserToGetKingdomsListFromPriestInput() {
        String kingdomsList = "kingdoms list";
        when(highPriest.getKingdomNominations()).thenReturn(kingdomsList);

        ballotSystemService.startRulerFindProcess();

        verifyStatic();
        Parser.getCompetingKingdomsFromPriestInput(kingdomsList);
    }

    @Test
    public void shouldContactParserToGetNonCompetingKingdoms() {
        String kingdomsList = "kingdoms list";
        when(highPriest.getKingdomNominations()).thenReturn(kingdomsList);

        ballotSystemService.startRulerFindProcess();

        verifyStatic();
        Parser.getNonCompetingKingdomsFromPriestInput(kingdomsList);
    }

    @Test
    public void shouldContactRandomMessageProviderForRandomMessages() {
        ballotSystemService.startRulerFindProcess();

        verifyStatic(atLeastOnce());
        RandomMessageProvider.get();
    }

    @Test
    public void shouldAddBallotMessagesToBallotWithRandomMessages() {
        ballotSystemService.startRulerFindProcess();

        verify(ballot, atLeastOnce()).addMessage(any(BallotMessage.class));
    }

    @Test
    public void shouldAskPriestToProcessBallot() {
        ballotSystemService.startRulerFindProcess();

        verify(highPriest).processBallot(ballot);
    }

    @Test
    public void shouldPrintAlliesCountOfEachCompetingAllie() {
        when(Parser.getCompetingKingdomsFromPriestInput(anyString()))
                .thenReturn(prepareMutableList(new Kingdom[]{competeKingdom, otherCompeteKingdom}));
        int competeKingdomAlliesCount = 3;
        when(competeKingdom.alliesCount()).thenReturn(competeKingdomAlliesCount);
        when(competeKingdom.toString()).thenReturn("Air");
        int otherCompeteKingdomAlliesCount = 4;
        when(otherCompeteKingdom.alliesCount()).thenReturn(otherCompeteKingdomAlliesCount);
        when(otherCompeteKingdom.toString()).thenReturn("Fire");

        ballotSystemService.startRulerFindProcess();

        verify(output).display("\nAllies for " + competeKingdom + ": " + competeKingdomAlliesCount + "\n");
        verify(output).display("\nAllies for " + otherCompeteKingdom + ": " + otherCompeteKingdomAlliesCount + "\n");
    }

    @Ignore
    public void shouldPrintMessageSayingResultsAfterBallotCount() {
        ballotSystemService.startRulerFindProcess();

        verify(output).display("\nResults after round 1 ballot count\n");
    }

    @Test
    public void shouldSortCompeteKingdomsDescendingBasedOnAlliesWonCount() {
        List<Kingdom> competeKingdoms = prepareMutableList(new Kingdom[]{competeKingdom, otherCompeteKingdom});
        setupForSortingCompeteKingdoms(competeKingdoms);

        ballotSystemService.startRulerFindProcess();

        Assert.assertSame(otherCompeteKingdom, competeKingdoms.get(0));
        Assert.assertSame(competeKingdom, competeKingdoms.get(1));
    }

    @Test
    public void shouldSetRulerIfThereIsNoTieAmongCompeteKingdoms() {
        List<Kingdom> competeKingdoms = prepareMutableList(new Kingdom[]{competeKingdom, otherCompeteKingdom});
        setupForSortingCompeteKingdoms(competeKingdoms);
        String winnerKingdomName = "Winner kingdom name";
        String wonAllies = "won allies";
        when(otherCompeteKingdom.toString()).thenReturn(winnerKingdomName);
        when(otherCompeteKingdom.allies()).thenReturn(wonAllies);

        ballotSystemService.startRulerFindProcess();

        Assert.assertEquals(winnerKingdomName, ballotSystemService.whoIsRuler());
        Assert.assertEquals(wonAllies, ballotSystemService.rulersAllies());
    }

    @Test
    public void shouldMoveNonTieKingdomsToNonCompeteKingdomsIfThereIsTie() {
        List<Kingdom> competeKingdoms = prepareMutableList(new Kingdom[]{competeKingdom, otherCompeteKingdom, anotherCompeteKingdom});
        List<Kingdom> nonCompeteKingdoms = prepareMutableList(new Kingdom[]{nonCompeteKingdom, otherNonCompeteKingdom});
        setupForSortingCompeteKingdoms(competeKingdoms);
        setupForNonCompeteKingdoms(nonCompeteKingdoms);
        when(competeKingdom.alliesCount())
                .thenReturn(3).thenReturn(3).thenReturn(3)
                .thenReturn(3).thenReturn(3).thenReturn(0);

        ballotSystemService.startRulerFindProcess();

        Assert.assertEquals(3, nonCompeteKingdoms.size());
        Assert.assertEquals(2, competeKingdoms.size());
        Assert.assertTrue(nonCompeteKingdoms.contains(anotherCompeteKingdom));
        Assert.assertTrue(competeKingdoms.contains(competeKingdom));
        Assert.assertTrue(competeKingdoms.contains(otherCompeteKingdom));
        Assert.assertFalse(competeKingdoms.contains(anotherCompeteKingdom));
    }

    @Test
    public void shouldNotProceedFurtherIfPriestInputIsEmpty() {
        when(highPriest.getKingdomNominations()).thenReturn("     ");

        ballotSystemService.startRulerFindProcess();

        verifyStatic(never());
        Parser.getCompetingKingdomsFromPriestInput(anyString());
        verifyStatic(never());
        Parser.getNonCompetingKingdomsFromPriestInput(anyString());
    }

    @Test
    public void shouldNotProceedFurtherIfParsedCompeteKingdomsAreEmpty() {
        when(Parser.getCompetingKingdomsFromPriestInput(anyString())).thenReturn(Collections.emptyList());

        ballotSystemService.startRulerFindProcess();

        verifyStatic(never());
        Parser.getNonCompetingKingdomsFromPriestInput(anyString());
    }

    private List<Kingdom> prepareMutableList(Kingdom[] kingdoms) {
        return new ArrayList<>(Arrays.asList(kingdoms));
    }

    private void setupForSortingCompeteKingdoms(List<Kingdom> competeKingdoms) {
        when(Parser.getCompetingKingdomsFromPriestInput(anyString()))
                .thenReturn(competeKingdoms);
        when(competeKingdom.alliesCount()).thenReturn(1);
        when(otherCompeteKingdom.alliesCount()).thenReturn(3);
    }

    private void setupForNonCompeteKingdoms(List<Kingdom> nonCompeteKingdoms) {
        when(Parser.getNonCompetingKingdomsFromPriestInput(anyString()))
                .thenReturn(nonCompeteKingdoms);
    }
}
