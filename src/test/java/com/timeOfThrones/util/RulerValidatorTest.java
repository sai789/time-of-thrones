package com.timeOfThrones.util;

import com.timeOfThrones.model.Kingdom;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RulerValidatorTest {

    @Mock
    private Kingdom kingdom;

    @Test
    public void shouldTrueIfMessageHasAllCharactersOfEmblem() {
        when(kingdom.getEmblem()).thenReturn("owl");

        boolean isValid = RulerValidator.isValidMessageForKingdom(kingdom, "oaaawaala");

        Assert.assertTrue(isValid);
    }

    @Test
    public void shouldFalseIfMessageDoesNotHaveAnyCharacterOfEmblem() {
        when(kingdom.getEmblem()).thenReturn("owl");

        boolean isValid = RulerValidator.isValidMessageForKingdom(kingdom, "oaaaaaala");

        Assert.assertFalse(isValid);
    }

    @Test
    public void shouldBeCaseInSensitiveWhileValidating() {
        when(kingdom.getEmblem()).thenReturn("oOwl");

        boolean isValid = RulerValidator.isValidMessageForKingdom(kingdom, "oaaaoaaWala");

        Assert.assertTrue(isValid);
    }

}
