package com.timeOfThrones.util;

import com.timeOfThrones.exception.UnknownKingdomException;
import com.timeOfThrones.model.Kingdom;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;

import static com.timeOfThrones.Constants.EXISTING_KINGDOMS;

public class ValidKingdomBuilderTest {

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void shouldGetKingdomIfNameMatchesExistingKingdomNames() {
        String kingdomName = "Air";

        Kingdom actualKingdom = ValidKingdomBuilder.getKingdomFor(kingdomName);

        Assert.assertEquals(kingdomName, actualKingdom.toString());
        Assert.assertEquals("Owl", actualKingdom.getEmblem());
    }

    @Test
    public void shouldBeCaseInSensitiveWhileMatchingKingdomName() {
        String kingdomName = "air";

        Kingdom actualKingdom = ValidKingdomBuilder.getKingdomFor(kingdomName);

        Assert.assertEquals("Air", actualKingdom.toString());
        Assert.assertEquals("Owl", actualKingdom.getEmblem());
    }

    @Test
    public void shouldExcludeTrailingSpacesWhileMatchingKingdomName() {
        String kingdomName = "     air   ";

        Kingdom actualKingdom = ValidKingdomBuilder.getKingdomFor(kingdomName);

        Assert.assertEquals("Air", actualKingdom.toString());
        Assert.assertEquals("Owl", actualKingdom.getEmblem());
    }

    @Test
    public void shouldThrowUnknownKingdomExceptionIfNameIsNotValid() {
        String nonExistingKingdomName = "Super";
        exception.expect(UnknownKingdomException.class);
        exception.expectMessage(CoreMatchers.equalTo(
                "Given kingdom " + nonExistingKingdomName + ", is not a valid kingdom name"));

        ValidKingdomBuilder.getKingdomFor(nonExistingKingdomName);
    }

    @Test
    public void shouldGetAllKingdomsOtherThanGiven() {
        String[] kingdoms = "Air,Fire,Water".split(",");

        List<Kingdom> otherKingdoms = ValidKingdomBuilder.getKingdomsOtherThan(kingdoms);

        Assert.assertEquals(EXISTING_KINGDOMS.size() - 3, otherKingdoms.size());
        Assert.assertTrue(otherKingdoms.contains(new Kingdom("Space", "")));
        Assert.assertTrue(otherKingdoms.contains(new Kingdom("Land", "")));
        Assert.assertTrue(otherKingdoms.contains(new Kingdom("Ice", "")));
    }
}
