package com.timeOfThrones.util;

import org.junit.Assert;
import org.junit.Test;

import static com.timeOfThrones.Constants.SECRET_MESSAGES;

public class RandomMessageProviderTest {
    @Test
    public void shouldPickRandomMessageFromList() {
        Assert.assertTrue(SECRET_MESSAGES.contains(RandomMessageProvider.get()));
    }
}
