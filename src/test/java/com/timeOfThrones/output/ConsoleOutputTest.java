package com.timeOfThrones.output;

import org.junit.Test;
import org.mockito.Mockito;

import java.io.PrintStream;

public class ConsoleOutputTest {
    @Test
    public void shouldCallPrintWithGivenMessage() {
        PrintStream printStream = Mockito.mock(PrintStream.class);
        Output output = new ConsoleOutput(printStream);
        String messageToDisplay = "message to display";

        output.display(messageToDisplay);

        Mockito.verify(printStream).print(messageToDisplay);
    }
}