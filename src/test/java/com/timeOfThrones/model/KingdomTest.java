package com.timeOfThrones.model;

import org.junit.Assert;
import org.junit.Test;

public class KingdomTest {
    @Test
    public void shouldGetNameOfKingdom() {
        Assert.assertEquals("Air", new Kingdom("Air", null).toString());
    }

    @Test
    public void shouldReturnTrueForTwoKingdomsWithSameNameComparision() {
        Assert.assertEquals(new Kingdom("Air", null), new Kingdom("Air", null));
    }

    @Test
    public void shouldGiveAlliesWithCommaSeparation() {
        Kingdom kingdom = new Kingdom(null, null);
        kingdom.wonAllie(new Kingdom("Ice", null));
        kingdom.wonAllie(new Kingdom("Fire", null));

        Assert.assertEquals("Ice, Fire", kingdom.allies());
    }
}
