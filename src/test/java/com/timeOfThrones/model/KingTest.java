package com.timeOfThrones.model;

import com.timeOfThrones.input.Input;
import com.timeOfThrones.util.ValidKingdomBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class KingTest {

    @Mock
    private Input input;

    @Test
    public void shouldGetNameOfKing() {
        String expectedKingName = "Shan";
        King shan = new King(expectedKingName, null);

        String actualKingName = shan.toString();

        Assert.assertEquals("King " + expectedKingName, actualKingName);
    }

    @Test
    public void shouldGiveRecipientAndSecretMessage() {
        King king = new King("", input);

        king.getRecipientAndSecretMessage();

        verify(input).get();
    }

    @Test
    public void shouldGiveFalseAsInitiallyKingCannotBeRuler() {
        King king = new King(null, null);

        Assert.assertFalse(king.isRuler());
    }

    @Test
    public void shouldGiveTrueIfRulerPropertyIsTrue() {
        King king = new King(null, null);
        king.setRuler(true);

        Assert.assertTrue(king.isRuler());
    }

    @Test
    public void shouldGiveAlliesWithCommaSeparation() {
        King king = new King(null, null);
        king.wonAllie(ValidKingdomBuilder.getKingdomFor("Ice"));
        king.wonAllie(ValidKingdomBuilder.getKingdomFor("Fire"));

        Assert.assertEquals("Ice, Fire", king.allies());
    }

    @Test
    public void shouldReduceSupportNeededCountByOneIfAllieAdded() {
        King king = new King(null, null, 9);

        king.wonAllie(ValidKingdomBuilder.getKingdomFor("Ice"));

        Assert.assertEquals(8, king.getKingdomSupportNeededCount());
    }

    @Test
    public void shouldNotAddAlreadyWonAllieAgain() {
        King king = new King(null, null);
        king.wonAllie(ValidKingdomBuilder.getKingdomFor("Ice"));

        king.wonAllie(ValidKingdomBuilder.getKingdomFor("Ice"));

        Assert.assertEquals("Ice", king.allies());
    }

}
