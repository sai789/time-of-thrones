package com.timeOfThrones.model;

import com.timeOfThrones.input.Input;
import com.timeOfThrones.util.RulerValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Optional;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;

@PrepareForTest(RulerValidator.class)
@RunWith(PowerMockRunner.class)
public class HighPriestTest {

    @Mock
    private Ballot ballot;

    @Mock
    private BallotMessage ballotMessage;

    @Mock
    private Kingdom kingdom;

    @Mock
    private Kingdom otherKingdom;

    @Mock
    private BallotMessage otherBallotMessage;

    @Mock
    private Input input;

    @Before
    public void setUp() {
        mockStatic(RulerValidator.class);
    }

    @Test
    public void shouldGiveNominatedKingdoms() {
        HighPriest highPriest = new HighPriest(input);

        highPriest.getKingdomNominations();

        verify(input).get();
    }

    @Test
    public void shouldPickMaximumSixBallotMessagesRandomly() {
        when(ballot.ballotSize()).thenReturn(8).thenReturn(7).thenReturn(6).thenReturn(5)
                .thenReturn(4).thenReturn(3).thenReturn(2).thenReturn(1).thenReturn(0);
        when(ballot.getMessageAt(anyInt())).thenReturn(Optional.of(ballotMessage));
        when(ballotMessage.getReceiver()).thenReturn(kingdom);
        HighPriest highPriest = new HighPriest(input);

        highPriest.processBallot(ballot);

        verify(ballot, times(6)).getMessageAt(anyInt());
    }

    @Test
    public void shouldContactRulerValidatorToValidateBallotMessages() {
        when(ballot.ballotSize()).thenReturn(2).thenReturn(1).thenReturn(0);
        when(ballot.getMessageAt(anyInt()))
                .thenReturn(Optional.of(ballotMessage))
                .thenReturn(Optional.of(otherBallotMessage));
        when(ballotMessage.getMessage()).thenReturn("ballotMessage");
        when(ballotMessage.getReceiver()).thenReturn(kingdom);
        when(otherBallotMessage.getMessage()).thenReturn("otherBallotMessage");
        when(otherBallotMessage.getReceiver()).thenReturn(otherKingdom);
        HighPriest highPriest = new HighPriest(input);

        highPriest.processBallot(ballot);

        verify(ballot, times(3)).ballotSize();
        verify(ballot, times(2)).getMessageAt(anyInt());
        verifyStatic();
        RulerValidator.isValidMessageForKingdom(kingdom, "ballotMessage");
        verifyStatic();
        RulerValidator.isValidMessageForKingdom(otherKingdom, "otherBallotMessage");
    }

    @Test
    public void shouldSetAllegationAndAlliesIfValidationTrue() {
        when(RulerValidator.isValidMessageForKingdom(any(), anyString())).thenReturn(true);
        when(ballot.ballotSize()).thenReturn(1).thenReturn(0);
        when(ballot.getMessageAt(anyInt())).thenReturn(Optional.of(ballotMessage));
        when(ballotMessage.getReceiver()).thenReturn(kingdom);
        when(ballotMessage.getSender()).thenReturn(otherKingdom);
        HighPriest highPriest = new HighPriest(input);

        highPriest.processBallot(ballot);

        verify(kingdom).setAllegianceGivenAlready(true);
        verify(otherKingdom).wonAllie(kingdom);
    }
}
