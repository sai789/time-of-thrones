package com.timeOfThrones.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class BallotTest {

    @Mock
    private BallotMessage message;

    @Mock
    private BallotMessage otherMessage;
    private Ballot ballot = new Ballot();

    @Before
    public void setUp() {
        ballot.addMessage(message);
        ballot.addMessage(otherMessage);
    }

    @Test
    public void shouldGiveOptionalEmptyMessageIfGivenIndexExceedsSize() {
        Optional<BallotMessage> pickedMessage = ballot.getMessageAt(200);

        Assert.assertFalse(pickedMessage.isPresent());
    }

    @Test
    public void shouldGiveOptionalEmptyMessageIfGivenIndexIsNegative() {
        Optional<BallotMessage> pickedMessage = ballot.getMessageAt(-1);

        Assert.assertFalse(pickedMessage.isPresent());
    }

    @Test
    public void shouldRemoveBallotMessageFromBallotOncePicked() {
        ballot.getMessageAt(1);
        Optional<BallotMessage> pickedMessage = ballot.getMessageAt(1);

        Assert.assertFalse(pickedMessage.isPresent());
    }
}