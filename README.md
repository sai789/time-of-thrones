# Time of Thrones - 1.0

This is a java console based application.
You will be acted as High Priest of Southeros and asked to enter what are all competing kingdoms(ex: Air Fire).

`Note:` Valid names are below kingdom names only. If you provide any wrong kingdom name, you will see `UnknownKingdomException`. This scenario is not being handled here in this problem. I felt ok to not handle here than making user misunderstand.

Problem statement(context):
----------------------------
There are 6 kingdoms in Universe of Southeros.


------------------------------
Kingdom   |  Emblem   |   King

------------------------------
Space     |  Gorilla   |  Shan

Air       |  Owl   |

Water     |  Octopus |

Land      |  Panda |

Ice       |  Mammoth |

Fire      |  Dragon |

---------------------------------

All the above kingdoms in the Universe yearn to be the ruler of Southeros and war is imminent!.The High Priest of Southeros
intervenes and is trying hard to avoid a war and he suggests a ballot system to decide the ruler.
This  coding challenge is to help the High Priest choose the ruler of Southeros through the ballot system.

`Rules of the Ballot system:`
 1. Any kingdom can compete to be the ruler.
 2. They should send a message to all other kingdoms asking for allegiance.
 3. This message will be put in a ballot from which the High Priest will pick 6 random messages.
 4. The High Priest then hands over the 6 messages to the respective receiving kingdoms.
 5. The kingdom that receives the highest number of allegiance is the ruler.
 
 `Rules to decide allegiance by a kingdom:`
  1. The receiving kingdom has to give allegiance to the sending kingdom if the message contains the letters of the animal in their
 emblem (same as problem 1).
  2. If the receiving kingdom is competing to be the ruler, they will not give their allegiance even if the message they received is correct.
  3. A kingdom cannot give their allegiance twice. If they have given their allegiance once, they will not give their allegiance again even
 if they get a second message and the message is correct.
 
 `In case there is a tie:`
 1. If there is a tie, the whole ballot process is repeated but only with the tied kingdoms till there is a winner
 
 `Sending messages:`
 
  The format of the message dropped in the ballot should contain :
  
 • The Sender kingdom
 • The Receiver kingdom
 • The Message (should be selected randomly from the messages provided in the table below) 

Runing evironment setup:
----------------------
Need gradle, java. Try below commands in project root path

`brew install gradle`

`brew cask install java8`

How to run:
------------

`gradle run`

How to build and run tests:
---------------------------

`gradle buildAndTest`

`gradle build`

`gradle test`